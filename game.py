from repository import getLastBiggestGameId
from repository import getMovesByGameId, getLastBiggestMoveId, saveMove
from repository import savePiece, getGame, getPiecesByMoveId, getLastBiggestPieceId, saveGame, updateGameProper
from tables import Game, Move, Piece


def createNewGame():
    game = Game.create()
    game.id = int(getLastBiggestGameId()) + 1
    saveGame(game)
    return game.id


def updateGame(gameId, nr):
    game = getGame(gameId)
    try:
        if game[0][1] == 'end':
            raise Exception('game already end')
    except (ValueError, IndexError):
        print("exception")

    moves = getMovesByGameId(gameId)

    for x in moves:
        if nr == x[2]:
            raise Exception('piece already taken')

    id = getLastBiggestMoveId() + 1
    move = createNewMove(gameId, nr, moves, id)

    saveMove(move)
    saveNewMap(move)

    map = []
    for x in getPiecesByMoveId(move.id):
        map.append(x[3])

    game = Game.create()
    game.id = move.game_id

    if sharpWon(map):
        game.circle = 0
        game.end = "end"
        updateGameProper(game)
    elif circleWon(map):
        game.circle = 1
        game.end = "end"
        updateGameProper(game)


def checkCoordinatesForSharp(map, c1, c2, c3):
    if map[c1 - 1] == 'sharp':
        if map[c2 - 1] == 'sharp':
            if map[c3 - 1] == 'sharp':
                return True
    return False


def checkCoordinatesForCircle(map, c1, c2, c3):
    if map[c1 - 1] == 'circle':
        if map[c2 - 1] == 'circle':
            if map[c3 - 1] == 'circle':
                return True
    return False


def saveNewMap(move):
    pieces = []
    pieceId = getLastBiggestPieceId() + 1

    if (move.round == 0):
        for x in range(0, 9):
            piece = Piece.create()
            piece.id = pieceId
            piece.nr = x + 1
            piece.type = "empty"
            piece.move_id = move.id

            pieceId += 1
            pieces.append(piece)

        column = int(move.nr) - 1
        if (isCircle(move.round)):
            pieces[column].type = "circle"
        else:
            pieces[column].type = "sharp"

    else:
        movies = getMovesByGameId(move.game_id)
        lastMovieId = 0
        for row in movies:
            if row[3] == int(move.round) - 1:
                lastMovieId = row[0]

        for row in getPiecesByMoveId(lastMovieId):
            if int(row[2]) != int(move.nr):
                piece = Piece.create()
                piece.id = pieceId
                piece.nr = row[2]
                piece.type = row[3]
                piece.move_id = move.id

                pieces.append(piece)
            else:
                piece = Piece.create()
                piece.id = pieceId
                piece.nr = move.nr
                piece.move_id = move.id

                if (isCircle(move.round)):
                    piece.type = "circle"
                else:
                    piece.type = "sharp"

                pieces.append(piece)

            pieceId += 1

    for piece in pieces:
        savePiece(piece)


def circleWon(map):
    if checkCoordinatesForCircle(map, 1, 2, 3):
        return True
    if checkCoordinatesForCircle(map, 4, 5, 6):
        return True
    if checkCoordinatesForCircle(map, 7, 8, 9):
        return True
    if checkCoordinatesForCircle(map, 1, 4, 7):
        return True
    if checkCoordinatesForCircle(map, 2, 5, 8):
        return True
    if checkCoordinatesForCircle(map, 3, 6, 9):
        return True
    if checkCoordinatesForCircle(map, 1, 5, 9):
        return True
    if checkCoordinatesForCircle(map, 7, 5, 3):
        return True

    return False


def sharpWon(map):
    if checkCoordinatesForSharp(map, 1, 2, 3):
        return True
    if checkCoordinatesForSharp(map, 4, 5, 6):
        return True
    if checkCoordinatesForSharp(map, 7, 8, 9):
        return True
    if checkCoordinatesForSharp(map, 1, 4, 7):
        return True
    if checkCoordinatesForSharp(map, 2, 5, 8):
        return True
    if checkCoordinatesForSharp(map, 3, 6, 9):
        return True
    if checkCoordinatesForSharp(map, 1, 5, 9):
        return True
    if checkCoordinatesForSharp(map, 7, 5, 3):
        return True

    return False


def createNewMove(gameId, nr, moves, moveId):
    move = Move.create()
    move.id = moveId
    move.game_id = gameId
    move.nr = nr

    if (moves == ()):
        move.circle = False
        move.counter = 1
    else:
        counter = 0
        for row in moves:
            if counter < row[3]:
                counter = row[3]

        counter += 1
        move.round = counter
        move.circle = isCircle(counter)

    return move


def isCircle(round):
    if round % 2 == 0:
        return False
    else:
        return True
