import MySQLdb
from time import sleep

sleep(10)
conn = MySQLdb.connect("db", "root", "admin", "test")
cursor = conn.cursor()

# Setup
def setup():
    try:
        cursor.execute("CREATE TABLE games (id int PRIMARY KEY,end varchar(255),circle int);")
        cursor.execute("CREATE TABLE moves (id int PRIMARY KEY,game_id int,nr int,round int,circle int);")
        cursor.execute("CREATE TABLE pieces (id int PRIMARY KEY,move_id int,nr int,type varchar(255));")
    except:
        print("Tables already exists")

''' SELECT '''

# For all
def getAllGames(id):
    cursor.execute("SELECT * FROM games")
    return cursor.fetchall()


def getAllMoves(id):
    cursor.execute("SELECT * FROM moves")
    return cursor.fetchall()


def getAllPieces(id):
    cursor.execute("SELECT * FROM pieces")
    return cursor.fetchall()


# For specific
def getGame(id):
    cursor.execute("SELECT * FROM games WHERE id = " + str(id))
    return cursor.fetchall()


def getMove(id):
    cursor.execute("SELECT * FROM moves WHERE id = " + str(id))
    return cursor.fetchall()


def getPiece(id):
    cursor.execute("SELECT * FROM pieces WHERE id = " + str(id))
    return cursor.fetchall()


# For dependencies
def getPiecesByMoveId(id):
    sql = "SELECT * FROM pieces WHERE pieces.move_id = " + str(id) + " ORDER BY pieces.nr ASC"
    cursor.execute(sql)
    return cursor.fetchall()


def getMovesByGameId(id):
    cursor.execute("SELECT * FROM moves WHERE moves.game_id = " + str(id))
    return cursor.fetchall()


def getMaxMovesIdByGameId(id):  # aehderdtfhdfc
    cursor.execute("SELECT MAX(moves.id) FROM moves WHERE moves.game_id = " + str(id))
    return cursor.fetchall()[0][0]


def getMovesByMoveRound(id):
    cursor.execute("SELECT * FROM moves WHERE moves.round = " + str(id))
    return cursor.fetchall()


def getMovesByMoveRoundAndForSpecificType(id, circleMove):
    sql = "SELECT * FROM moves WHERE moves.round = " + str(id) + " AND moves.circle = " + isCircle(circleMove)
    cursor.execute(sql)
    return cursor.fetchall()


def isCircle(type):
    if type == True:
        return "1"
    else:
        return "0"


# getLastBiggest
def getLastBiggestGameId():
    cursor.execute("SELECT MAX(games.id) FROM `games`")
    data = cursor.fetchall()[0][0]
    if (data == None):
        return 0
    else:
        return data


def getLastBiggestMoveId():
    cursor.execute("SELECT MAX(moves.id) FROM `moves`")
    data = cursor.fetchall()[0][0]
    if (data == None):
        return 0
    else:
        return data


def getLastBiggestPieceId():
    cursor.execute("SELECT MAX(pieces.id) FROM `pieces`")
    data = cursor.fetchall()[0][0]
    if (data == None):
        return 0
    else:
        return data


def getMaxRoundByGameId(id):  # here
    cursor.execute("SELECT MAX(round) FROM moves WHERE moves.game_id = " + str(id))
    return cursor.fetchall()


def getNextRoundNr(move):  # here
    cursor.execute(
        "SELECT nr FROM moves WHERE moves.game_id = " + str(move[1]) + " AND moves.round = " + str(int(move[3] + 1)))
    return cursor.fetchall()[0][0]


''' INSERT '''


def savePiece(piece):
    cursor.execute("INSERT INTO `pieces` (`id`, `move_id`, `nr`, `type`) VALUES (" + str(piece.id) + ', ' + str(
        piece.move_id) + ", " + str(piece.nr) + ", '" + str(piece.type) + "')")
    conn.commit()


def saveMove(move):
    cursor.execute(
        "INSERT INTO `moves` (`id`, `game_id`, `nr`, `round`, `circle`) VALUES (" + str(move.id) + ', ' + str(
            move.game_id) + ", " + str(move.nr) + ", " + str(move.round) + ", " + str(move.circle) + ")")
    conn.commit()


def saveGame(game):
    cursor.execute(
        "INSERT INTO `games` (`id`, `end`, `circle`) VALUES (" + str(game.id) + ', ' + str(game.end) + ", " + str(
            game.circle) + ")")
    conn.commit()


''' UPDATE '''


def updateGameProper(game):
    print(game.end)
    sql = "UPDATE `games` SET `end`='" + str(game.end) + "', `circle`=" + str(game.circle) + " WHERE `id`=" + str(
        game.id)
    cursor.execute(sql)
    conn.commit()


''' DELETE '''


def deleteAll():
    cursor.execute("DELETE FROM `games`; DELETE FROM `moves`; DELETE FROM `pieces`; ")
    conn.commit()
