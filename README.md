## Description
Simple AI for tic tac toe - backend

### How to run with docker(demo) :
- make sure your ports 8080 and 5000 are free
- copy docker-compose.yml file from this repository to your local environment
- locate docker-compose.yml file and run command in same directory : docker-compose up
- get onto localhost:8080/index.html
