class Game:
    id = 0
    end = 0
    circle = 0

    @staticmethod
    def create():
        return Game()

    def incrementId(self):
        self.id += 1


class Move:
    id = 0
    game_id = 0
    nr = 0  # change to coordinates
    round = 0
    circle = False

    @staticmethod
    def create():
        return Move()


class Piece:
    id = 0
    move_id = 0
    nr = 0
    type = ''

    @staticmethod
    def create():
        return Piece()
