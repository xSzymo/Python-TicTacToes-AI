FROM python:2.7

RUN pip install Flask
RUN pip install mysql-connector-python
RUN pip install flask-cors --upgrade
RUN pip install flask-restful
RUN pip install MySQL-python

WORKDIR /app

COPY . /app

ENV NAME World

CMD ["python", "mvc.py"]
