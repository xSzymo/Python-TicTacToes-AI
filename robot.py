from repository import getMaxRoundByGameId, getNextRoundNr, getGame
from repository import getMovesByGameId, getMovesByMoveRoundAndForSpecificType, getPiecesByMoveId, getMaxMovesIdByGameId

x = {'value': 10}


def getRobotMove(gameId):
    gameId = long(gameId)
    currentMoves = getMovesByGameId(gameId)
    moveId = getMaxMovesIdByGameId(gameId)
    round = getNumberRound(currentMoves)

    if len(currentMoves) < 1:
        actuallMap = ["empty", "empty", "empty", "empty", "empty", "empty", "empty", "empty", "empty"]
    else:
        actuallMap = convertPieceToMap(getPiecesByMoveId(moveId))

    moves = getMovesByMoveRoundAndForSpecificType(round - 1, not isCircleMove(currentMoves))
    moves = getMovesWithSameMapAsGiven(moves, actuallMap)
    moves = filterFromCurrentGame(moves, gameId)
    moves = filterFromNonEndGames(moves)

    winners = collectUnique(getMovesWinners(moves, round))
    losers = collectUnique(getMovesLosers(moves, round))
    noImpact = collectUnique(filterFakes(getMovesNoImpact(moves, round), winners, losers))

    if len(winners) + len(losers) + len(noImpact) > 9 - (round + 1):
        return getBestLearnedMove(moves, winners, noImpact, losers, actuallMap, round)
    else:
        return createNewMove(winners, losers, noImpact, actuallMap)


def createNewMove(winners, losers, noImpact, actuallMap):
    winnersNr = []
    loosersNr = []
    noImpactNr = []

    for winner in winners:
        nr = getNextRoundNr(winner)
        if nr not in winnersNr:
            winnersNr.append(nr)

    for loser in losers:
        nr = getNextRoundNr(loser)
        if nr not in loosersNr:
            loosersNr.append(nr)

    for noImpac in noImpact:
        nr = getNextRoundNr(noImpac)
        if nr not in loosersNr:
            noImpactNr.append(nr)

    for i in range(1, 10):
        if actuallMap[i - 1] == "empty":
            if i not in noImpactNr:
                if i not in loosersNr:
                    if i not in winnersNr:
                        return i

    raise Exception('A very specific bad thing happenesfdcafw3qfgw3qegtw4gw4d.')


def getBestLearnedMove(moves, winners, noImpact, losers, actuallMap, round):
    if len(winners) > 0:
        return getNextRoundNr(winners[0])

    if len(noImpact) > 0:
        return getNextRoundNr(noImpact[0])
    else:
        print "dEGWSKGHSKHTDRKJHSDRKJNSKHRKJNHRKJNHDKJNFHTRXHFXFHXFHHTRKJNHDXFHFTCJHNGJCFGTYJ"
        return getNextRoundNr(losers[0])


def collectUnique(moves):
    unique = []
    for move in moves:
        if not move[2] in unique:
            unique.append(move)
    return unique


def getMovesNoImpact(moves, round):
    noImpact = []
    for move in moves:
        if getMaxRoundByGameId(move[1])[0][0] > round + 1:
            noImpact.append(move)

    return noImpact


def filterFakes(moves, winners, losers):
    filtered = []
    winnersNr = []
    loosersNr = []

    for winner in winners:
        nr = getNextRoundNr(winner)
        if nr not in winnersNr:
            winnersNr.append(nr)

    for loser in losers:
        nr = getNextRoundNr(loser)
        if nr not in loosersNr:
            loosersNr.append(nr)

    for move in moves:
        nr = getNextRoundNr(move)
        if nr not in winnersNr:
            if nr not in loosersNr:
                filtered.append(move)

    return moves


def getMovesLosers(moves, round):
    loosers = []
    for move in moves:
        if getMaxRoundByGameId(move[1])[0][0] == round + 1:
            loosers.append(move)

    return loosers


def getMovesWinners(moves, round):
    winners = []
    for move in moves:
        if getMaxRoundByGameId(move[1])[0][0] == round:
            winners.append(move)

    return winners


def filterFromCurrentGame(moves, gameId):
    filtered = []
    for move in moves:
        if move[1] != gameId:
            filtered.append(move)

    return filtered


def filterFromNonEndGames(moves):
    filtered = []
    for move in moves:
        if getGame(move[1])[0][1] == "end":
            filtered.append(move)

    return filtered


def getMovesWithSameMapAsGiven(moves, map):
    newMoves = []
    for move in moves:
        if areSame(convertPieceToMap(getPiecesByMoveId(move[0])), map):
            newMoves.append(move)

    return newMoves


def areSame(map, map1):
    for i in range(0, 9):
        if map[i] != map1[i]:
            return False

    return True


def getLastMoveId(moves, round):
    for move in moves:
        if move[3] == round:
            return move[0]


def convertPieceToMap(pieces):
    map = []

    for piece in pieces:
        if piece[3] == "circle":
            map.append("circle")
        elif piece[3] == "sharp":
            map.append("sharp")
        else:
            map.append("empty")

    return map


def isCircleMove(moves):
    counter = 0

    for row in moves:
        if counter < row[3]:
            counter = row[3]

    if counter % 2 == 0:
        return True
    else:
        return False


def getNumberRound(moves):
    counter = 0

    for row in moves:
        if counter < row[3]:
            counter = row[3]

    return counter + 1
