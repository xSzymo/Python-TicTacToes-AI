from flask import Flask
from flask_cors import CORS
from flask_restful import reqparse, Api, Resource

from game import createNewGame, updateGame
from robot import getRobotMove
from repository import setup

app = Flask(__name__)
CORS(app)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('nr')
parser.add_argument('id')


class AI(Resource):
    def post(self):
        move = getRobotMove(parser.parse_args()['id'])
        updateGame(parser.parse_args()['id'], move)
        return move

    def put(self):
        updateGame(parser.parse_args()['id'], parser.parse_args()['nr'])

    def get(self):
        return createNewGame()


class Trained(Resource):
    def post(self):
        return "dupa"


api.add_resource(AI, '/learn')
api.add_resource(Trained, '/get')

if __name__ == '__main__':
    setup()
    app.run(host='0.0.0.0', port=5000)
